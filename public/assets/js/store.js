"Use strict";

fetch("./assets/products/products.json")
  .then(result => result.json())
  .then(data => renderStore(data))

function renderStore(data) {
  const root = document.querySelector('.store');
  const elements = data.map((product, index) => createStoreElement(product, index + 1));

  elements.forEach((element) => root.append(element));
}

function createStoreElement(product, id) {
  const root = document.createElement('div');
  root.className = `store__item-${id}`;
  root.setAttribute('data-code', product.code)

  const link = document.createElement('a');
  link.className = 'store__link'; // link.setAttribute('class',  'store__link')
  link.href = '#?code=' + product.code ;

  const cell = document.createElement('div');
  cell.className = 'cell';
  link.append(cell);

  const cellPrice = document.createElement('div');
  cellPrice.className = 'cell__price';

  const {
    oldPrice,
    newPrice
  } = calcPrice(product.price, product.discount);

  const cellPriceOld = document.createElement('p');
  cellPriceOld.className = 'cell__price-old';
  cellPriceOld.innerText = oldPrice ? `RUB ${oldPrice}` : '';

  const cellPriceNow = document.createElement('p');
  cellPriceNow.className = 'cell__price-now';
  cellPriceNow.innerText = `RUB ${newPrice}`;

  cellPrice.append(cellPriceOld, cellPriceNow);

  const cellImage = document.createElement('img');
  cellImage.className = 'cell__img';
  cellImage.src = `./assets/products/${product.picture}`;

  const cellMerch = document.createElement('p');
  cellMerch.className = 'cell__merch';
  cellMerch.innerText = product.name;

  const cellBtn = document.createElement('button');
  cellBtn.className = 'cell__btn';
  cellBtn.innerText = 'Быстрый просмотр';

  cell.append(cellImage, cellMerch, cellPrice);

  root.append(link);

  return root;
}


function calcPrice(price, discount) {
  if (discount === 0) {
    return {
      oldPrice: null,
      newPrice: price
    }
  }

  return {
    oldPrice: price,
    newPrice: price * (100 - discount) / 100,
  };
}