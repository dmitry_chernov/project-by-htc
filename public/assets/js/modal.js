"Use Strcict";


// let html = document.documentElement;
// let scrollPosition = window.pageYOffset;

// html.style.top = -scrollPosition + "px";
// html.classList.add("modal__opened");

// html.classList.remove("modal__opened");
// window.scrollTo(0, scrollPosition);
// html.style.top = "";

// Основной класс

class modal {

  constructor(props) {

    let defaultConfig = {
      linkAttributeName: 'data-modal'
    };

    this.config = Object.assign(defaultConfig, props);
    this.init();
  }

  static _shadow = false;

  init() {

    this.isOpened = false;
    this.openedWindow = false;
    this._modalBlock = false;
    this.starter = false;
    this._nextWindows = false;
    this._scrollPosition = 0;

    if (!modal._shadow) {
      modal._shadow = document.createElement('div');
      modal._shadow.classList.add('modal__shadow');
      document.body.appendChild(modal._shadow);
    }

    this.eventsFeeler();
  }

  eventsFeeler() {

    document.addEventListener("click", function(e) {
      
      const clickedlink = e.target.closest("[" + this.config.linkAttributeName + "]");

      if (clickedlink) {
        e.preventDefault();
        this.starter = clickedlink;
        let targetSelector = this.starter.getAttribute(this.config.linkAttributeName);
        this._nextWindows = document.querySelector(targetSelector);
        this.open();
        return;
      }

      if (e.target.closest('[data-modalclose]')) {
        this.close();
        return;
      }
    }

    .bind(this));
  }

  open(selector) {

    this.openedWindow = this._nextWindows;
    this._nextWindows = this.openedWindow.querySelector('.modal__window');
    this._bodyScrollControl();
    modal._shadow.classList.add("modal__shadow--show");
    this.openedWindow.classList.add("modal--active");
    this.openedWindow.setAttribute('aria-hidden', 'false');

    this.focusControl();
    this.isOpened = true;
  }

  close() {

    if (!this.isOpened) {
      return;
    }

    this.openedWindow.classList.remove("modal--active");
    modal._shadow.classList.remove('modal__shadow--show');
    this.openedWindow.setAttribute('aria-hidden', 'true');
    this.focusControl();
    this._bodyScrollControl();
    this.isOpened = false;
  }

  // _bodyScrollControl() {
    
  //   let html = document.documentElement;
    
  //   if(this.isOpened === true) {
  //     html.classList.remove("modal__opened");
  //     html.style.marginRight = "";
  //     window.scrollTo(0, this._scrollPosition);
  //     html.style.top = "";
  //     return;
  //   }

  //   this._scrollPosition = window.pageYOffset;
  //   html.style.top = -this._scrollPosition + "px";
  //   html.classList.add("modal__opened");

  // }
}

const myModal = new Modal ({
  linkAttributeName: 'data-modal'
});